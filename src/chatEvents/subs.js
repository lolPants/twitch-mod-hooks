// Package Dependencies
const snekfetch = require('snekfetch')
const humanize = require('humanize-plus')

// Local Dependencies
const { getFromName } = require('../helpers/getUserInfo')

// Environment Variables
const { CHAT_WEBHOOK_URL } = process.env

const bits = client => {
  client.on('subscription', (channel, username, method, message) => { handleSub(channel, username, method, message) })
  client.on('resub', (channel, username, months, message, userstate, method) => { handleSub(channel, username, method, message, months) })
}

const handleSub = async (channel, username, method, message, months = null) => {
  try {
    let users = await getFromName(username)
    let user = users[0]

    let subText = `${user.display_name} has subscribed`
    if (method.prime) subText += ' with Twitch Prime'
    if (months !== null) subText += ` for ${months} ${humanize.pluralize(months, 'month')}`
    subText += '!'

    let fields = []
    if (message !== null) {
      fields.push(
        {
          title: 'Subscription Message',
          value: message,
          short: true,
        }
      )
    }

    // Post to webhook
    await snekfetch.post(CHAT_WEBHOOK_URL)
      .send({
        icon_url: user.profile_image_url,
        username: user.display_name,
        attachments: [
          {
            pretext: subText,
            color: '#7058a6',
            fields,
          },
        ],
      })
  } catch (err) {
    console.error(err)
  }
}

module.exports = bits
