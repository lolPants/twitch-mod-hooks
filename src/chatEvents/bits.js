// Package Dependencies
const snekfetch = require('snekfetch')
const humanize = require('humanize-plus')

// Local Dependencies
const { getFromID } = require('../helpers/getUserInfo')

// Environment Variables
const { CHAT_WEBHOOK_URL, BITS_WEBHOOK_URL } = process.env

const bits = client => {
  client.on('cheer', async (channel, state, message) => {
    try {
      let users = await getFromID(state['user-id'])
      let user = users[0]

      // Parse bit amount
      let amount = parseInt(state.bits)

      let payload = {
        text: message,
        icon_url: user.profile_image_url,
        username: user.display_name,
        attachments: [
          {
            thumb_url: bitURL(amount),
            color: '#7058a6',
            fields: [
              {
                title: 'Cheer Amount',
                value: `${humanize.intComma(amount)} ${humanize.pluralize(amount, 'bit')}`,
                short: true,
              },
            ],
          },
        ],
      }

      // Post to webhook(s)
      let hooks = [snekfetch.post(CHAT_WEBHOOK_URL).send(payload)]
      if (BITS_WEBHOOK_URL !== undefined) hooks.push(snekfetch.post(BITS_WEBHOOK_URL).send(payload))
      await Promise.all(hooks)
    } catch (err) {
      console.error(err)
    }
  })
}

const bitURL = amount => {
  amount = parseInt(amount)
  if (amount >= 100000) return 'https://i.imgur.com/m55nCQt.png'
  else if (amount >= 10000) return 'https://i.imgur.com/Jr7QJlr.png'
  else if (amount >= 5000) return 'https://i.imgur.com/mbI8BY7.png'
  else if (amount >= 1000) return 'https://i.imgur.com/jLGQK4z.png'
  else if (amount >= 100) return 'https://i.imgur.com/8M9sDJq.png'
  else return 'https://i.imgur.com/xVgR9aG.png'
}

module.exports = bits
