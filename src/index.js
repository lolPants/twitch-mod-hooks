// Package Dependencies
const log = require('fancylog')

// Local Dependencies
const modHooks = require('./modHooks')
const twitchChat = require('./twitchChat')

// Enviroment Variables
const { DISABLE_MOGLOG } = process.env

// Start Modlog
if (DISABLE_MOGLOG === undefined) modHooks()
else log.w('Twitch Mod Log Disabled')

// Call Twitch Chat Helpers
twitchChat()
