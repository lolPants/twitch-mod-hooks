// Package Dependencies
const moment = require('moment')

/**
 * @param {string} timeout Timeout in Seconds
 * @returns {string}
 */
const timeoutParser = timeout => {
  let value = parseInt(timeout) < 60 ?
    `${moment.duration(timeout * 1000).seconds()} seconds` :
    `${timeout} seconds (${moment.duration(timeout * 1000).humanize()})`
  return value
}

module.exports = timeoutParser
