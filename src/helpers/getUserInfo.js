// Package Dependencies
const snekfetch = require('snekfetch')

// Environment Variables
const { CLIENT_ID } = process.env

/**
 * @typedef {Object} TwitchUser
 * @property {string} id
 * @property {string} login
 * @property {string} display_name
 * @property {string} type
 * @property {string} broadcaster_type
 * @property {string} description
 * @property {string} profile_image_url
 * @property {string} offline_image_url
 * @property {number} view_count
 */

/**
 * @param {string[]} users Array of User IDs
 * @returns {Promise.<TwitchUser[]>}
 */
const getFromID = async users => {
  if (typeof users === 'string') users = [users]
  try {
    let data = await snekfetch.get(`https://api.twitch.tv/helix/users?${users.map(x => `id=${x}`).join('&')}`)
      .set('Client-ID', CLIENT_ID)
    return data.body.data
  } catch (err) {
    throw err
  }
}

/**
 * @param {string[]} users Array of User IDs
 * @returns {Promise.<TwitchUser[]>}
 */
const getFromName = async users => {
  if (typeof users === 'string') users = [users]
  try {
    let data = await snekfetch.get(`https://api.twitch.tv/helix/users?${users.map(x => `login=${x}`).join('&')}`)
      .set('Client-ID', CLIENT_ID)
    return data.body.data
  } catch (err) {
    throw err
  }
}

module.exports = { getFromName, getFromID }
