/**
 * @param {string} type Twitch Action Type
 * @returns {string}
 */
const typeEmoji = type => {
  switch (type) {
    case 'timeout':
      return '🕒'
    case 'ban':
      return '⛔'
    case 'unban':
      return '✅'
    case 'slow':
      return '🏁'
    case 'slowoff':
      return '⏩'
    case 'mod':
      return '⚙'
    case 'unmod':
      return '⚙'
    default:
      return '❓'
  }
}

module.exports = typeEmoji
