// Package Dependencies
const googl = require('goo.gl')

// Environment Variables
const { LISTEN_NAME, RELAY_CHANNEL_ID, SHORT_URL_KEY } = process.env

// URL Shortener
googl.setKey(SHORT_URL_KEY)

const outgoingChat = (client, bot) => {
  // Handle Discord Messages
  bot.on('message', async message => {
    // Check that messages are sent in the right channel
    if (message.channel.id !== RELAY_CHANNEL_ID) return false

    // Check messages are from users only
    if (message.author.bot) return false

    // Send message to twitch chat
    let text = `${message.author.username}: ${message.cleanContent}`

    // Check for attachments
    if (message.attachments.array().length > 0) {
      // Grab all URLs
      let urls = message.attachments.map(x => x.url)
      // Shorten all URLs
      let shortened = await Promise.all(urls.map(x => googl.shorten(x)))

      // Map to string and add to text
      let attachmentString = shortened.map(x => `Image: ${x}`).join(' ')
      text += ` - ${attachmentString}`
    }

    client.say(LISTEN_NAME, text)
  })
}

module.exports = outgoingChat
