const EventEmitter = require('events')

/**
 * Message Buffer
 */
class MessageBuffer extends EventEmitter {
  /**
   * Message Buffer Emitter
   * @param {number} [interval] Millisecond interval between clearing messages (default = 1000ms)
   */
  constructor (interval = 1000) {
    super()
    this._store = []

    // Interval
    setInterval(() => {
      // Copy the current contents of the array
      let copy = this._store.slice(0)

      // Remove from array
      this._store.splice(0, copy.length)

      // Fire Event
      this.emit('push', copy)
    }, interval)
  }

  /**
   * Push data to the buffer
   * @param {*} data Input Data
   */
  push (data) {
    this._store.push(data)
  }
}

module.exports = MessageBuffer
