// Package Dependencies
const dateFormat = require('dateformat')

// Local Dependencies
const MessageBuffer = require('./MessageBuffer.js')

// Environment Variables
const { RELAY_CHANNEL_ID, BUFFER_TIMEOUT } = process.env

// Badge Constants
const MOD_EMOJI = '<:moderator:366358840943312898>'
const BROADCASTER_EMOJI = '<:broadcaster:366358841236914186>'

/**
 * @typedef {Object} Message
 * @property {string} channel
 * @property {string} message
 * @property {boolean} self
 * @property {Object} state
 */

const incomingChat = (client, bot) => {
  // Define new Buffer
  let timeout = parseInt(BUFFER_TIMEOUT) || 1000
  const buffer = new MessageBuffer(timeout)

  client.on('chat', (channel, state, message, self) => {
    if (self) return false
    else buffer.push({ channel, state, message, self })
  })

  client.on('action', (channel, state, message, self) => {
    if (self) return false
    else buffer.push({ channel, state, message: `*${message}*`, self })
  })

  buffer.on('push', data => {
    /**
     * @type {Message[]}
     */
    let a = data

    try {
      // Only handle if new messages have arrived
      if (a.length !== 0) {
        // Map the raw objects
        let text = a.map(x => {
          // Grab used info
          let { state, message } = x
          // Define output string
          let str = ''

          // Grab Timestamp
          const ms = parseInt(state['tmi-sent-ts'])
          let date = new Date(ms)
          let dateStr = dateFormat(date, 'hh:MM:ss')

          str += `\`[${dateStr}]\` `

          // Handle Chat Badges
          if (state.badges !== null) {
            if (state.badges.broadcaster === '1') str += `${BROADCASTER_EMOJI} `
            if (state.badges.moderator === '1') str += `${MOD_EMOJI} `
          }

          // Construct Message
          str += `**${state['display-name']}:** ${message}`
          // Return
          return str
        })

        // Define Discord Channel
        let ch = bot.channels.get(RELAY_CHANNEL_ID)
        // Send all messages to the text channel
        ch.send(text.join('\n'))
      }
    } catch (err) { console.error(err) }
  })
}

module.exports = incomingChat
