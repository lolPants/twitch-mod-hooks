// Package Dependencies
const log = require('fancylog')
const WebSocket = require('ws')
const snekfetch = require('snekfetch')
const humanize = require('humanize-plus')

// Environment Variables
const { LISTEN_NAME, AUTH_TOKEN, AUTH_NAME, MOD_WEBHOOK_URL } = process.env

// Local Dependencies
const { getFromName, getFromID } = require('./helpers/getUserInfo')
const typeEmoji = require('./helpers/typeEmoji')
const timeoutParser = require('./helpers/timeoutParser')

const modHooks = () => {
  const ws = new WebSocket('wss://pubsub-edge.twitch.tv/')

  ws.on('open', async () => {
    // Show Message and Ping
    log.i('Connected')
    ws.send(JSON.stringify({ type: 'PING' }))

    // Ping Interval
    setInterval(() => {
      ws.send(JSON.stringify({ type: 'PING' }), err => {
        if (err) {
          log.e(`Ping Error: ${err.message}`)
          console.error(err)
        }
      })
    }, 1000 * 60 * 2)

    // Get Channels to Subscribe
    let IDs = await getFromName([AUTH_NAME, LISTEN_NAME])
    let login = IDs[0]
    let listener = IDs[1]

    // Subscribe
    let payload = {
      type: 'LISTEN',
      data: {
        topics: [`chat_moderator_actions.${login.id}.${listener.id}`],
        auth_token: AUTH_TOKEN,
      },
    }
    ws.send(JSON.stringify(payload))
  })

  /**
   * @typedef {Object} Payload
   * @property {string[]} args
   * @property {string} created_by
   * @property {string} created_by_user_id
   * @property {string} moderation_action
   * @property {string} msg_id
   * @property {string} target_user_id
   * @property {string} type
   */

  ws.on('message', async data => {
    let json = JSON.parse(data)

    try {
      if (json.type === 'MESSAGE') {
        /**
         * @type {Payload}
         */
        let payload = JSON.parse(json.data.message).data

        /**
         * @type {string}
         */
        let channelID = json.data.topic.split('.')[2]

        // Fetch User Info
        let userArr = [channelID, payload.created_by_user_id]
        if (payload.target_user_id !== '') userArr.push(payload.target_user_id)
        let users = await getFromID(userArr)

        // Assign Users
        let channel = users[0]
        let moderator = users[1]
        let target = users[2]

        // Add Action Field
        let fields = [
          {
            title: 'Action',
            value: `${typeEmoji(payload.moderation_action)} ${humanize.capitalize(payload.moderation_action)}`,
            short: true,
          },
        ]

        // Switch fields based on action
        if (payload.moderation_action === 'timeout') {
          let value = timeoutParser(payload.args[1])
          value = `⌛ ${value}`

          fields.push(
            {
              title: 'Length',
              value,
              short: true,
            },
            {
              title: 'Target',
              value: `👤 [${target.display_name}](https://twitch.tv/${target.login})`,
              short: true,
            }
          )

          if (payload.args[2] !== undefined) {
            fields.push(
              {
                title: 'Reason',
                value: `🗒 ${payload.args[2]}`,
                short: true,
              },
            )
          }
        } else if (payload.moderation_action === 'ban' || payload.moderation_action === 'unban') {
          fields.push(
            {
              title: 'Target',
              value: `👤 [${target.display_name}](https://twitch.tv/${target.login})`,
              short: true,
            }
          )
        } else if (payload.moderation_action === 'slow') {
          let value = timeoutParser(payload.args[0])
          value = `⏰ ${value}`

          fields.push(
            {
              title: 'Slowmode Timer',
              value,
              short: true,
            }
          )
        } else if (payload.moderation_action === 'mod' || payload.moderation_action === 'unmod') {
          fields.push(
            {
              title: 'Target',
              value: `👤 [${target.display_name}](https://twitch.tv/${target.login})`,
              short: true,
            }
          )
        }

        // Post to webhook
        await snekfetch.post(MOD_WEBHOOK_URL)
          .send({
            text: '',
            attachments: [
              {
                author_name: moderator.display_name,
                author_link: `https://twitch.tv/${moderator.login}`,
                author_icon: moderator.profile_image_url,
                color: '#7058a6',
                title: `Moderator Action in ${channel.display_name}`,
                fields,
                ts: (new Date).getTime() / 1000,
              },
            ],
          })
      }
    } catch (err) {
      // Handle Errors
      console.error(err)
    }
  })
}

module.exports = modHooks
