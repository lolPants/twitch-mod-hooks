// Package Dependencies
const exitHook = require('async-exit-hook')
const log = require('fancylog')
const tmi = require('tmi.js')
const discord = require('discord.js')

// Setup bot
const bot = new discord.Client()

// Local Dependencies
const incomingChat = require('./chatBridge/incomingChat')
const outgoingChat = require('./chatBridge/outgoingChat')
const bits = require('./chatEvents/bits')
const subs = require('./chatEvents/subs')

// Environment Variables
const { LISTEN_NAME, AUTH_TOKEN, AUTH_NAME, DISABLE_BRIDGE, DISABLE_BITS, DISABLE_SUBS, DISCORD_TOKEN } = process.env

const twitchChat = () => {
  try {
    // Setup IRC Client
    const client = new tmi.Client({
      options: {
        debug: true,
      },
      connection: {
        reconnect: true,
      },
      identity: {
        username: AUTH_NAME,
        password: `oauth:${AUTH_TOKEN}`,
      },
      channels: [`#${LISTEN_NAME}`],
    })

    // Connect to IRC
    client.connect()

    // Login to Discord
    bot.login(DISCORD_TOKEN)

    // Set Statuses
    bot.on('ready', () => {
      // Random Statuses
      let URL = 'https://twitch.tv/nerdcubed'
      let statuses = [
        'Twiddlies!',
        'Dog cam',
        'Dan\'s insanity',
        'not the Podcats :(',
        'Nerd³ Online... oh wait',
      ]

      const setStatus = () => { bot.user.setActivity(statuses[Math.floor(Math.random() * statuses.length)], { url: URL, type: 'STREAMING' }) }
      setStatus()
      setInterval(setStatus, 1000 * 10)
    })

    // Twitch - Discord Bridge
    if (DISABLE_BRIDGE === undefined) {
      // Start chat bridge
      incomingChat(client, bot)
      outgoingChat(client, bot)
    } else {
      log.w('Twitch Chat Bridge Disabled')
    }

    // Bits Events
    if (DISABLE_BITS === undefined) bits(client)
    else log.w('Twitch Bits Logging Disabled')

    // Sub Events
    if (DISABLE_SUBS === undefined) subs(client)
    else log.w('Twitch Sub Logging Disabled')

    /**
     * Logout Function
     * Handles CTRL+C and PM2
     */
    exitHook(async exit => {
      // Check if logged in
      if (bot.readyAt !== null) {
        try {
          await bot.destroy()
          exit()
        } catch (err) { exit() }
      } else { exit() }
    })
  } catch (err) {
    console.error(err)
  }
}

module.exports = twitchChat
